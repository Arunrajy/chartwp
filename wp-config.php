<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
@ini_set( 'upload_max_size' , '15M' );
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'chart_wp_db' );
/** MySQL database username */
define( 'DB_USER', 'root' );
/** MySQL database password */
define( 'DB_PASSWORD', '' );
/** MySQL hostname */
define( 'DB_HOST', 'localhost' );
/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );
/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=@EmO+!G!{fa3<T7ewi]U.)+hLw2Q@+L}-,>VPYe0$4`fuU:`voJ37tUD$#fiS/<');
define('SECURE_AUTH_KEY',  '$:-HUqYFF-p5sy34@ff(/Tdm?inBwAu**mF|l3xWR3:Q+R/|G8s,<5Z,.xbLBf36');
define('LOGGED_IN_KEY',    'f+u:v.Z(_jF)vb.gOn%[lB!h 3:i.~|D!E3%5;qUe>E]:el0*]Hl`AYwTU+<!Boh');
define('NONCE_KEY',        'RZD=)2V!/@(W)#|RWQ@mWBOtw{SCn*&*SFi&scFYp:+aD)i%,IAqJ:$k.*JI!lK(');
define('AUTH_SALT',        ']kp,GH#%*3!0D~{B}xQ~4!H!o>K{ek}oDelJ2a{4hy^zg,|_JtXoeg(VV18g7;J:');
define('SECURE_AUTH_SALT', 'G5QETB168|i-sOx!x-YH)u]-y3)G9CI%~7]5ldc$_l|,P0<=yh2MW9:]:m(x G+s');
define('LOGGED_IN_SALT',   '|T7-ORpPJb,P}l0+65j/.SY-Fva$(|^9BT%`Z6cx1RqIdm}scU-$5+P~54(8fme`');
define('NONCE_SALT',       'B_ crRmO<yH#6 aYiVpfW!j!Y^+6}=Zc,3/z4Y3E>Ui2!-V]flL+1%ENn$6UlIgI');
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'rt_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define( 'DISALLOW_FILE_EDIT', true );
/* That's all, stop editing! Happy publishing. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
