<?php

/**
 * Fired during plugin activation
 *
 * @link       asd
 * @since      1.0.0
 *
 * @package    Chartgen
 * @subpackage Chartgen/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Chartgen
 * @subpackage Chartgen/includes
 * @author     sadasd <asdadad>
 */
class Chartgen_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
