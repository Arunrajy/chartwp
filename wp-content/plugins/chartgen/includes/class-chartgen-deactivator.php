<?php

/**
 * Fired during plugin deactivation
 *
 * @link       asd
 * @since      1.0.0
 *
 * @package    Chartgen
 * @subpackage Chartgen/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Chartgen
 * @subpackage Chartgen/includes
 * @author     sadasd <asdadad>
 */
class Chartgen_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
