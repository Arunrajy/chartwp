<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       asd
 * @since      1.0.0
 *
 * @package    Chartgen
 * @subpackage Chartgen/admin/partials
 */

if ( isset( $_POST['submit'] ) ){

    global $wpdb;


    $tablename=$wpdb->prefix.'customchart';

    $data=array(
        'chart_type' => $_POST['chartgentype'],
        'chart_data' => $_POST['chartgendatas'],
        'chart_labelname' => $_POST['chartgenlabelname'],
        'chart_labels' => $_POST['chartgenlabels'],
        'chart_backgroundcolors' => $_POST['chartgenbackgroundcolors'],
        'chart_bordercolors' => $_POST['chartgenbordercolors'],
        'chart_borderwidth' => $_POST['chartgenborderwidth'],
        'shortcode' => $_POST['chartgenshortcode'],
        'ID' => $_POST['chartgenid'],
);

// Required field names
$required = array('chartgenid','chartgentype', 'chartgendatas', 'chartgenlabelname', 'chartgenlabels','chartgenbackgroundcolors', 'chartgenbordercolors', 'chartgenborderwidth', 'chartgenshortcode');

// Loop over field names, make sure each one exists and is not empty
$error = false;
foreach($required as $field) {
  if (empty($_POST[$field])) {
    $error = true;
  }
}

if ($error) {
  echo "<div class='alert alert-danger' role='alert'>All fields are required.</div>";
} else {
  var_dump($tablename);
  var_dump($data);
     $wpdb->insert( $tablename, $data);
  echo "<div class='alert alert-success' role='alert'>Successfully Posted...</div>";
}
}
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
    <?php
    $active_tab = 'all_charts';
if( isset( $_GET[ 'tab' ] ) ) {
  $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'all_charts';
} // end if
?>
<h2 class="nav-tab-wrapper">
  <a href="?page=chartgen&tab=all_charts" class="nav-tab <?php echo $active_tab == 'all_charts' ? 'nav-tab-active' : ''; ?>">All Charts</a>
  <a href="?page=chartgen&tab=add_charts" class="nav-tab <?php echo $active_tab == 'add_charts' ? 'nav-tab-active' : ''; ?>">Add Charts</a>
  <a href="?page=chartgen&tab=chart_options" class="nav-tab <?php echo $active_tab == 'chart_options' ? 'nav-tab-active' : ''; ?>">Chart Options</a>
</h2>


      <?php
          //Grab all options
          $options = get_option($this->plugin_name);
          // Cleanup
          $cleanup = $options['cleanup'];
          $comments_css_cleanup = $options['comments_css_cleanup'];
          $gallery_css_cleanup = $options['gallery_css_cleanup'];
          $body_class_slug = $options['body_class_slug'];
          $jquery_cdn = $options['jquery_cdn'];
          $cdn_provider = $options['cdn_provider'];
      ?>

      <?php
        if( $active_tab == 'all_charts' ) {
          // settings_fields($this->plugin_name);
          // do_settings_sections($this->plugin_name);

          ?>
          <div class="panel panel-default">
              <div class="panel-heading">Panel Header</div>
              <div class="panel-body">Panel Content</div>
            </div>
        <?php
      }else if( $active_tab == 'add_charts' ) {
        ?>
        <!-- <div class="panel panel-default">
            <div class="panel-heading">Panel Header</div>
            <div class="panel-body">Panel Content</div>
          </div> -->

<form action="" id="postchart" method="post">
    <table class="table custom_table">
      <tr>
          <td><label for="chartgenid">Chart ID:</label></td>
          <td><input type="text" name="chartgenid" id="chartgenid" readonly
            /></td>
      </tr>

        <tr>
            <td><label for="chartgentype">Chart Type:</label></td>
            <td>
              <select name="chartgentype" id="chartgentype">
              <option value="line">Line Chart</option>
              <option value="bar">Bar Chart</option>
              <option value="pie">Pie Chart</option>

              </select>
            </td>
        </tr>
        <tr>
            <td><label for="chartgendatas">Chart Datas:</label></td>
            <td><input type="text" name="chartgendatas" id="chartgendatas" value="" placeholder="eg: value, value, value, ....."/></td>
        </tr>
        <tr>
            <td><label for="chartgenlabelname">Chart Label Name</label></td>
            <td><input type="text" name="chartgenlabelname" id="chartgenlabelname" placeholder="Enter the Label Name"/></td>
        </tr>
        <tr>
            <td><label for="chartgenlabels">Chart Labels</label></td>
            <td><input type="text" name="chartgenlabels" id="chartgenlabels" placeholder="eg: String, String, String, ...."/></td>
        </tr>
        <tr>
            <td><label for="chartgenbackgroundcolors">Chart Background Color</label></td>
            <td><input type="text" name="chartgenbackgroundcolors" id="chartgenbackgroundcolors"  placeholder="eg: rgba(000,000,000,0.5), #000000, ......" /></td>
        </tr>
        <tr>
            <td><label for="chartgenbordercolors">Chart Border Color</label></td>
            <td><input type="text" name="chartgenbordercolors" id="chartgenbordercolors"  placeholder="eg: rgba(000,000,000,0.5), #000000, ......"/></td>
        </tr>
        <tr>
            <td><label for="chartgenborderwidth">Chart Border Width</label></td>
            <td><input type="text" name="chartgenborderwidth" id="chartgenborderwidth"  placeholder="enter border width from 0 to 10"/></td>
        </tr>
        <tr>
            <td><label for="chartgenshortcode" >Chart Shortcode</label></td>
            <td><textarea type="text" name="chartgenshortcode"  readonly="readonly"  id="chartgenshortcode"  placeholder="Automated Shortcode"></textarea></td>
        </tr>
        <tr>
            <td><button class="btn btn-primary" type="submit" name="submit">Submit</button></td>
        </tr>
    </table>
</form>
      <?php
      }else {
          // settings_fields($this->plugin_name);
          // do_settings_sections($this->plugin_name);

          ?>



              <form method="post" name="cleanup_options" action="options.php">
            <!-- remove some meta and generators from the <head> -->
            <fieldset>
                <legend class="screen-reader-text"><span>Clean WordPress head section</span></legend>
                <label for="<?php echo $this->plugin_name; ?>-cleanup">
                    <input type="checkbox" id="<?php echo $this->plugin_name; ?>-cleanup" name="<?php echo $this->plugin_name; ?>[cleanup]" value="1"/>
                    <span><?php esc_attr_e('Clean up the head section', $this->plugin_name); ?></span>
                </label>
            </fieldset>

            <!-- remove injected CSS from comments widgets -->
            <fieldset>
                <legend class="screen-reader-text"><span>Remove Injected CSS for comment widget</span></legend>
                <label for="<?php echo $this->plugin_name; ?>-comments_css_cleanup">
                    <input type="checkbox" id="<?php echo $this->plugin_name; ?>-comments_css_cleanup" name="<?php echo $this->plugin_name; ?>[comments_css_cleanup]" value="1"/>
                    <span><?php esc_attr_e('Remove Injected CSS for comment widget', $this->plugin_name); ?></span>
                </label>
            </fieldset>

            <!-- remove injected CSS from gallery -->
            <fieldset>
                <legend class="screen-reader-text"><span>Remove Injected CSS for galleries</span></legend>
                <label for="<?php echo $this->plugin_name; ?>-gallery_css_cleanup">
                    <input type="checkbox" id="<?php echo $this->plugin_name; ?>-gallery_css_cleanup" name="<?php echo $this->plugin_name; ?>[gallery_css_cleanup]" value="1" />
                    <span><?php esc_attr_e('Remove Injected CSS for galleries', $this->plugin_name); ?></span>
                </label>
            </fieldset>

            <!-- add post,page or product slug class to body class -->
            <fieldset>
                <legend class="screen-reader-text"><span><?php _e('Add Post, page or product slug to body class', $this->plugin_name); ?></span></legend>
                <label for="<?php echo $this->plugin_name; ?>-body_class_slug">
                    <input type="checkbox" id="<?php echo $this->plugin_name;?>-body_class_slug" name="<?php echo $this->plugin_name; ?>[body_class_slug]" value="1" />
                    <span><?php esc_attr_e('Add Post slug to body class', $this->plugin_name); ?></span>
                </label>
            </fieldset>

            <!-- load jQuery from CDN -->
            <fieldset>
                <legend class="screen-reader-text"><span><?php _e('Load jQuery from CDN instead of the basic wordpress script', $this->plugin_name); ?></span></legend>
                <label for="<?php echo $this->plugin_name; ?>-jquery_cdn">
                    <input type="checkbox"  id="<?php echo $this->plugin_name; ?>-jquery_cdn" name="<?php echo $this->plugin_name; ?>[jquery_cdn]" value="1" />
                    <span><?php esc_attr_e('Load jQuery from CDN', $this->plugin_name); ?></span>
                </label>
                        <fieldset>
                            <p>You can choose your own cdn provider and jQuery version(default will be Google Cdn and version 1.11.1)-Recommended CDN are <a href="https://cdnjs.com/libraries/jquery">CDNjs</a>, <a href="https://code.jquery.com/jquery/">jQuery official CDN</a>, <a href="https://developers.google.com/speed/libraries/#jquery">Google CDN</a> and <a href="http://www.asp.net/ajax/cdn#jQuery_Releases_on_the_CDN_0">Microsoft CDN</a></p>
                            <legend class="screen-reader-text"><span><?php _e('Choose your prefered cdn provider', $this->plugin_name); ?></span></legend>
                            <input type="url" class="regular-text" id="<?php echo $this->plugin_name; ?>-cdn_provider" name="<?php echo $this->plugin_name; ?>[cdn_provider]" value=""/>
                        </fieldset>
            </fieldset>

            <?php submit_button('Save all changes', 'primary','submit', TRUE); ?>
            </form>
<?php
        }
             // submit_button();
      ?>



</div>
<script>
var randomnumber=Math.round(Math.random()*(1000 - 10 + 1)) + 10;
console.log(randomnumber);
// set random id
document.getElementById('chartgenid').value=randomnumber;

//get values from input field
var chart_gen_type = document.getElementById('chartgentype');

var chart_gen_datas = document.getElementById('chartgendatas');
var chart_gen_labelname = document.getElementById('chartgenlabelname');
var chart_gen_labels = document.getElementById('chartgenlabels');
var chart_gen_backgroundcolors = document.getElementById('chartgenbackgroundcolors');
var chart_gen_bordercolors = document.getElementById('chartgenbordercolors');
var chart_gen_borderwidth = document.getElementById('chartgenborderwidth');

var createshortcode = document.getElementById('chartgenshortcode');
chartgenid.addEventListener('change', updateValue);
chart_gen_type.addEventListener('change', updateValue);
chart_gen_datas.addEventListener('change', updateValue);
chart_gen_labelname.addEventListener('change', updateValue);
chart_gen_labels.addEventListener('change', updateValue);
chart_gen_backgroundcolors.addEventListener('change', updateValue);
chart_gen_bordercolors.addEventListener('change', updateValue);
chart_gen_borderwidth.addEventListener('change', updateValue);

function updateValue(e) {
  console.log(e.target.value);
  console.log(chart_gen_datas.textContent);


createshortcode.value="[chartgen id='chart_"+randomnumber+"'";
createshortcode.value +=" charttype='"+chart_gen_type.value+"'";
createshortcode.value +=" data='"+chart_gen_datas.value+"'";
createshortcode.value +=" labels='"+chart_gen_labels.value+"'";
createshortcode.value +=" backgroundcolors='"+chart_gen_backgroundcolors.value+"'";
createshortcode.value +=" bordercolors='"+chart_gen_bordercolors.value+"'";
createshortcode.value +=" labelname='"+chart_gen_labelname.value+"'";
createshortcode.value +=" borderwidth='"+chart_gen_borderwidth.value+"'";
createshortcode.value +=" ]";
}
console.log(chart_gen_datas.textContent);

// $("[type='text']#chartgenid").val("adsasd");
</script>
