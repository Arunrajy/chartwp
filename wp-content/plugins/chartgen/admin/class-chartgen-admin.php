<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       asd
 * @since      1.0.0
 *
 * @package    Chartgen
 * @subpackage Chartgen/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chartgen
 * @subpackage Chartgen/admin
 * @author     sadasd <asdadad>
 */
class Chartgen_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chartgen_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chartgen_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chartgen-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chartgen_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chartgen_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( 'customjs', plugin_dir_url( __FILE__ ) . 'js/jquery-3.4.1.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chartgen-admin.js', array( 'jquery' , 'customjs'), $this->version, false );



	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */

	public function add_plugin_admin_menu() {

	    /*
	     * Add a settings page for this plugin to the Settings menu.
	     *
	     * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
	     *
	     *        Administration Menus: http://codex.wordpress.org/Administration_Menus
	     *
	     */
	    add_menu_page( 'Chartgen Options Functions Setup', 'Chartgen', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page')
	    );
			add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
			 // add_submenu_page($this->plugin_name, 'Settings page title', 'Settings menu label', 'manage_options', 'theme-op-settings', 'wps_theme_func_settings');
		 // add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
		 $hook = add_submenu_page(
			 $this->plugin_name,
			 'Sitepoint WP_List_Table Example',
			 'List of Chartgen',
			 'manage_options',
			 'wp_list_table_class',
			 array( $this, 'plugin_settings_page' )
		 );

		 add_action( "load-$hook", [ $this, 'screen_option' ] );
		 $this-> pmc_define();
		 $this-> pmc_db_create();
	}
	 /**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	 public function pmc_define(){
		 global $wpdb;
		 define('CUSTOMCHART_TABLE', $wpdb->prefix . 'customchart');
	 }

	 public function pmc_db_create() {
		 global $wpdb;
			 $charset_collate = $wpdb->get_charset_collate();
			 $table_name = CUSTOMCHART_TABLE;

			 $sql = "CREATE TABLE $table_name (
				 ID mediumint(9) ZEROFILL NOT NULL AUTO_INCREMENT,
				 chart_type varchar(600) ,
				 chart_data varchar(600) ,
				 chart_labelname varchar(600) ,
				 chart_labels varchar(600) ,
				 chart_backgroundcolors varchar(600) ,
				 chart_bordercolors varchar(600) ,
				 chart_borderwidth varchar(600) ,
				 shortcode varchar(600) ,
				 UNIQUE KEY ID (ID)
			 ) $charset_collate;";

			 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			 dbDelta( $sql );
	 }
	public function add_action_links( $links ) {
	    /*
	    *  Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
	    */
	   $settings_link = array(
	    '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' . __('Settings', $this->plugin_name) . '</a>',
	   );
	   return array_merge(  $settings_link, $links );

	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */

	public function display_plugin_setup_page() {
	    include_once( 'partials/chartgen-admin-display.php' );
	}

	public function options_update() {
     register_setting($this->plugin_name, $this->plugin_name, array($this, 'validate'));
  }

	public function validate($input) {
    // All checkboxes inputs
    $valid = array();

    //Cleanup
    $valid['cleanup'] = (isset($input['cleanup']) && !empty($input['cleanup'])) ? 1 : 0;
    $valid['comments_css_cleanup'] = (isset($input['comments_css_cleanup']) && !empty($input['comments_css_cleanup'])) ? 1: 0;
    $valid['gallery_css_cleanup'] = (isset($input['gallery_css_cleanup']) && !empty($input['gallery_css_cleanup'])) ? 1 : 0;
    $valid['body_class_slug'] = (isset($input['body_class_slug']) && !empty($input['body_class_slug'])) ? 1 : 0;
    $valid['jquery_cdn'] = (isset($input['jquery_cdn']) && !empty($input['jquery_cdn'])) ? 1 : 0;
    $valid['cdn_provider'] = esc_url($input['cdn_provider']);

    return $valid;
 }
 // class instance
 static $instance;

 // customer WP_List_Table object
 public $customers_obj;

 // class constructor
 // public function __construct() {
 //
 // }


 public static function set_screen( $status, $option, $value ) {
	 return $value;
 }

 public function plugin_menu() {



 }


 /**
	* Plugin settings page
	*/
 public function plugin_settings_page() {
	 ?>
	 <div class="wrap">
		 <h2>List of Chartgen</h2>

		 <div id="poststuff">
			 <div id="post-body" class="metabox-holder columns-2">
				 <div id="post-body-content">
					 <div class="meta-box-sortables ui-sortable">
						 <form method="post">
							 <?php
							 $this->customers_obj->prepare_items();
							 $this->customers_obj->display(); ?>
						 </form>
					 </div>
				 </div>
			 </div>
			 <br class="clear">
		 </div>
	 </div>
 <?php
 }

 /**
	* Screen options
	*/
 public function screen_option() {

	 $option = 'per_page';
	 $args   = [
		 'label'   => 'Total Custom Charts',
		 'default' => 5,
		 'option'  => 'customers_per_page'
	 ];

	 add_screen_option( $option, $args );

	 $this->customers_obj = new Customers_List();
 }


 /** Singleton instance */
 public static function get_instance() {
	 if ( ! isset( self::$instance ) ) {
		 self::$instance = new self();
	 }

	 return self::$instance;
 }
}


if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Customers_List extends WP_List_Table {

	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => __( 'Customer', 'sp' ), //singular name of the listed records
			'plural'   => __( 'Customers', 'sp' ), //plural name of the listed records
			'ajax'     => false //does this table support ajax?
		] );

	}


	/**
	 * Retrieve customers data from the database
	 *
	 * @param int $per_page
	 * @param int $page_number
	 *
	 * @return mixed
	 */
	public static function get_customers( $per_page = 5, $page_number = 1 ) {

		global $wpdb;

		$sql = "SELECT * FROM {$wpdb->prefix}customchart";

		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}

		$sql .= " LIMIT $per_page";
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}


	/**
	 * Delete a customer record.
	 *
	 * @param int $id customer ID
	 */
	public static function delete_customer( $id ) {
		global $wpdb;

		$wpdb->delete(
			"{$wpdb->prefix}customchart",
			[ 'ID' => $id ],
			[ '%d' ]
		);
	}


	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
	public static function record_count() {
		global $wpdb;

		$sql = "SELECT COUNT(*) FROM {$wpdb->prefix}customchart";

		return $wpdb->get_var( $sql );
	}


	/** Text displayed when no customer data is available */
	public function no_items() {
		_e( 'No customchart avaliable.', 'sp' );
	}


	/**
	 * Render a column when no column specific method exist.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'ID':
			case 'chart_type':
			case 'chart_data':
			case 'chart_labelname':
			case 'chart_labels':
			case 'chart_backgroundcolors':
			case 'chart_bordercolors':
			case 'chart_borderwidth':
			case 'shortcode':
				return preg_replace('/\\\\/', '', $item[ $column_name ]);;
			default:
				return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}
	}

	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
		);
	}


	/**
	 * Method for name column
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_name( $item ) {

		$delete_nonce = wp_create_nonce( 'sp_delete_customer' );

		$title = '<strong>' . $item['name'] . '</strong>';

		$actions = [
			'edit' => sprintf( '<a href="?page=%s&action=%s&customer=%s&_wpnonce=%s">Edit</a>', esc_attr( $_REQUEST['page'] ), 'edit', absint( $item['ID'] ), $delete_nonce ),

			'delete' => sprintf( '<a href="?page=%s&action=%s&customer=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['ID'] ), $delete_nonce )
		];

		return $title . $this->row_actions( $actions );
	}


	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {
		$columns = [
			'cb'      => '<input type="checkbox" />',
			'ID'    => __( 'Id', 'sp' ),
			'chart_type'    => __( 'Chart Type', 'sp' ),
			'chart_data' => __( 'Chart Data', 'sp' ),
			'chart_labelname'    => __( 'Chart Label Name', 'sp' ),
			'chart_labels'    => __( 'Chart Label ', 'sp' ),
			'chart_backgroundcolors'    => __( 'Chart Background Color ', 'sp' ),
			'chart_bordercolors'    => __( 'Chart Border Color', 'sp' ),
			'chart_borderwidth'    => __( 'Chart Border Width', 'sp' ),
			'shortcode'    => __( 'Shortcode', 'sp' )
		];

		return $columns;
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
			'ID' => array( 'ID', true ),
			'chart_type' => array( 'chart_type', true ),
			'chart_data' => array( 'chart_data', true ),
			'chart_labelname' => array( 'chart_labelname', true ),
			'chart_labels' => array( 'chart_labels', true ),
			'chart_backgroundcolors' => array( 'chart_backgroundcolors', true ),
			'chart_bordercolors' => array( 'chart_bordercolors', true ),
			'chart_borderwidth' => array( 'chart_borderwidth', true ),
			'shortcode' => array( 'shortcode', true )
		);

		return $sortable_columns;
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			'bulk-delete' => 'Delete'
		];

		return $actions;
	}


	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( 'customers_per_page', 5 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );

		$this->items = self::get_customers( $per_page, $current_page );
	}

	public function process_bulk_action() {

		//Detect when a bulk action is being triggered...
		if ( 'delete' === $this->current_action() ) {

			// In our file that handles the request, verify the nonce.
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );

			if ( ! wp_verify_nonce( $nonce, 'sp_delete_customer' ) ) {
				die( 'Go get a life script kiddies' );
			}
			else {
				self::delete_customer( absint( $_GET['customchart'] ) );

		                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		                // add_query_arg() return the current url
		                wp_redirect( esc_url_raw(add_query_arg()) );
				exit;
			}

		}

		// If the delete bulk action is triggered
		if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		     || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		) {

			$delete_ids = esc_sql( $_POST['bulk-delete'] );

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_customer( $id );

			}

			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		        // add_query_arg() return the current url
		        wp_redirect( esc_url_raw(add_query_arg()) );
			exit;
		}
	}

}
