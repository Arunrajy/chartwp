<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       asd
 * @since      1.0.0
 *
 * @package    Chartgen
 * @subpackage Chartgen/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Chartgen
 * @subpackage Chartgen/public
 * @author     sadasd <asdadad>
 */
class Chartgen_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->_addShortcode( $this->plugin_name, 'getChartData_func' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chartgen_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chartgen_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chartgen-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chartgen_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chartgen_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		 wp_enqueue_script( 'Chart', plugin_dir_url( __FILE__ ) . 'js/Chart.min.js', array( 'jquery' ), $this->version, false );
		 wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chartgen-public.js', array( 'jquery' ), $this->version, false );


	}


	protected function _addShortcode( $tag, $method ) {
		add_shortcode( $tag, array( $this, $method ) );
		return $this;
	}
	public function getChartData_func($atts) {

  // $Content = "<style>\r\n";
  // $Content .= "h3.demoClass {\r\n";
  // $Content .= "color: #26b158;\r\n";
  // $Content .= "}\r\n";
  // $Content .= "</style>\r\n";
  // $Content .= '<h3 class="demoClass">Check it out!</h3>';
	//
  //    return $Content;
	global $wpdb;
	$tablename=$wpdb->prefix.'customchart';
	if(!empty($atts)){

		preg_match_all('!\d+!', $atts['id'], $matches);
		$chart_id = implode(' ', $matches[0]);

$count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $tablename WHERE ID = %d", $chart_id));
	$results = $wpdb->get_results( "SELECT * FROM $tablename");
	//if $count 1 means true else if 0 means false
if($count == 1 ){
	// foreach($results as $row){
	//
	// 	if($atts['id'] = $row->ID  ){
			$chartdata = array(
			 'labels' => explode(',', $atts['labels']),
			 'datasets' => array(
				 'data' => explode(',', $atts['data']),
				 'backgroundColor' => array_map(function($str) {
						 return str_replace('-', ',', $str);
				 },explode(',', $atts['backgroundcolors'])),
				 'borderColor' => array_map(function($str) {
						 return str_replace('-', ',', $str);
				 },explode(',', $atts['bordercolors'])),
				 'label' => $atts['labelname'],
				 'borderWidth' =>$atts['borderwidth']
			 ),
			 'type' => $atts['charttype'],
			 'chart_id' => $atts['id']
			);
				 $Content = '<canvas id="'.$chartdata["chart_id"].'" width="400" height="400" style="margin:0 auto"></canvas>';
				 $Content .= "<script type='text/javascript'>\r\n";
				 $Content .= "var chart_data =".json_encode($chartdata)."\r\n";
				 $Content .= "console.log(chart_data);\r\n";
				 $Content .= "if(chart_data!= null){\r\n";
				 $Content .= "var ctx = document.getElementById(chart_data.chart_id).getContext('2d');\r\n";
				 $Content .= "var myChart = new Chart(ctx, {\r\n";
				 $Content .= "type: chart_data.type,\r\n";
				 $Content .= "data: {\r\n";
				 $Content .= "labels: chart_data.labels,\r\n";
				 $Content .= "datasets: [chart_data.datasets]\r\n";
				 $Content .= "},\r\n";
				 $Content .= "options: {\r\n";
				 $Content .= "scales: {\r\n";
				 $Content .= "yAxes: [{\r\n";
				 $Content .= "ticks: {\r\n";
				 $Content .= "beginAtZero: true\r\n";
				 $Content .= "}\r\n";
				 $Content .= "}]\r\n";
				 $Content .= "}\r\n";
				 $Content .= "}\r\n";
				 $Content .= "});\r\n";
				 $Content .= "}\r\n";
				 $Content .= "</script>\r\n";


				 return $Content;
		}else{

	 return "<div style='padding-top:20px;text-align:center; font-size:16px; background-color:#ff5757; padding:10px;'> ID ".$chart_id." is not a valid Chartgen ID. Please Check the ID is Registed in Chartgen plugin or Not</div>";


		}
	}
	}


}
