<?php
/**
 * Plugin Name: PMC WP List Table
 * Plugin URI:  https://premiumCoding.com
 * Description: Wordpress List Table example.
 * Version:     1.0
 * Author:      PremiumCoding
 * Author URI:  https://premiumCoding.com
 * License:     GPLv3
 */




/**
 * This is the PMC  loader class.
 *
 * @package PMC WP List Table
 */
if ( ! class_exists( 'PMC_Loader_WP_List_Table' ) ) {


	class PMC_Loader_WP_List_Table {


		/**
		 * Start up
		 */
		public function __construct()
		{

			add_action( 'admin_menu', array( $this, 'pmc_add_plugin_page' ) );
			$this-> pmc_define();
			$this-> pmc_db_create();


		}

		public function pmc_define(){
			global $wpdb;
			define('PMC_FS_TABLE', $wpdb->prefix . 'pmc_fs');
		}

		public function pmc_db_create() {
			global $wpdb;
				$charset_collate = $wpdb->get_charset_collate();
				$table_name = PMC_FS_TABLE;

				$sql = "CREATE TABLE $table_name (
					id mediumint(9) NOT NULL AUTO_INCREMENT,
					name varchar(255) ,
					last_name varchar(255) ,
					email varchar(255) ,
					action varchar(255) ,
					UNIQUE KEY id (id)
				) $charset_collate;";

				require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
				dbDelta( $sql );
		}

		public function pmc_add_plugin_page()
		{

				/* add pages & menu items */
			add_menu_page( esc_attr__( 'PMC WP List Table', 'textdomain' ),esc_html__( 'PMC List Table', 'textdomain' ),
			'administrator','pmc-listTable',array( $this, 'pmc_create_admin_page' ), '', 10);

		}



		/**
		 * Admin page callback
		 */
		public function pmc_create_admin_page()
		{
			global $wpdb;
			?>
			<div class="wrap pmc-fs">
			<?php
				$pmc_fs_table = new PMC_FS_WP_List_Table();
				echo '<div class="wrap"><h2>Addandoned Cart list</h2>';
				$pmc_fs_table->prepare_items();
				echo '<input type="hidden" name="page" value="" />';
				echo '<input type="hidden" name="section" value="issues" />';

				$pmc_fs_table->views();
				echo '<form method="post">';
				echo ' <input type="hidden" name="page" value="pmc_fs_search">';
				$pmc_fs_table->search_box( 'search', 'search_id' );
				$pmc_fs_table->display();
				echo '</form></div>';

				echo '</div>';

		}

	}


	if( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}


	class PMC_FS_WP_List_Table extends WP_List_Table
	{

		function __construct(){
			parent::__construct( array(
				'ajax'      => false        //does this table support ajax?
		) );

		}


		/**
		 * Add columns to grid view
		 */
		function get_columns(){
			$columns = array(
			'name' => 'Name',
			'last_name'    => 'Last Name',
			'email'      => 'Email',
			'action'	=> 'Action'
			);
			return $columns;
		}

		function column_default( $item, $column_name ) {
			switch( $column_name ) {
				case 'id':
				case 'name':
				case 'last_name':
				case 'email':
				case 'action':
			  return $item[ $column_name ];
			default:
			  return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
			}
		}

		protected function get_views() {
		  $views = array();
		   $current = ( !empty($_REQUEST['customvar']) ? $_REQUEST['customvar'] : 'all');

		   //All link
		   $class = ($current == 'all' ? ' class="current"' :'');
		   $all_url = remove_query_arg('customvar');
		   $views['all'] = "<a href='{$all_url }' {$class} >All</a>";

		   //Recovered link
		   $foo_url = add_query_arg('customvar','recovered');
		   $class = ($current == 'recovered' ? ' class="current"' :'');
		   $views['recovered'] = "<a href='{$foo_url}' {$class} >Recovered</a>";

		   //Abandon
		   $bar_url = add_query_arg('customvar','abandon');
		   $class = ($current == 'abandon' ? ' class="current"' :'');
		   $views['abandon'] = "<a href='{$bar_url}' {$class} >Abandon</a>";

		   return $views;
		}



		function usort_reorder( $a, $b ) {
		  // If no sort, default to title
		  $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'id';
		  // If no order, default to asc
		  $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'desc';
		  // Determine sort order
		  $result = strcmp( $a[$orderby], $b[$orderby] );
		  // Send final sort direction to usort
		  return ( $order === 'asc' ) ? $result : -$result;
		}

		function get_sortable_columns() {
			$sortable_columns = array(
			'action'  => array('action',false),
			);
			return $sortable_columns;
		}

		/**
		 * Prepare admin view
		 */
		function prepare_items() {
			global $wpdb;

			$per_page = 50;
			$current_page = $this->get_pagenum();
			if ( 1 < $current_page ) {
				$offset = $per_page * ( $current_page - 1 );
			} else {
				$offset = 0;
			}

			$search = '';
			//Retrieve $customvar for use in query to get items.
			$customvar = ( isset($_REQUEST['customvar']) ? $_REQUEST['customvar'] : '');
			if($customvar != '') {
				$search_custom_vars= "AND action LIKE '%" . esc_sql( $wpdb->esc_like( $customvar ) ) . "%'";
			} else	{
				$search_custom_vars = '';
			}
			if ( ! empty( $_REQUEST['s'] ) ) {
				$search = "AND email LIKE '%" . esc_sql( $wpdb->esc_like( $_REQUEST['s'] ) ) . "%'";
			}

			$items = $wpdb->get_results( "SELECT id,name,last_name,email,action FROM ".PMC_FS_TABLE." WHERE 1=1 {$search} {$search_custom_vars}" . $wpdb->prepare( "ORDER BY id DESC LIMIT %d OFFSET %d;", $per_page, $offset ),ARRAY_A);
			$columns = $this->get_columns();
			$hidden = array();
			$sortable = $this->get_sortable_columns();
			$this->_column_headers = array($columns, $hidden, $sortable);
			usort( $items, array( &$this, 'usort_reorder' ) );
			$count = $wpdb->get_var( "SELECT COUNT(id) FROM ".PMC_FS_TABLE." WHERE 1 = 1 {$search} {$search_custom_vars}" );

			$this->items = $items;

			// Set the pagination
			$this->set_pagination_args( array(
				'total_items' => $count,
				'per_page'    => $per_page,
				'total_pages' => ceil( $count / $per_page )
			) );
		}


	}

}




  /**
   * Instantiate the loader class.
   *
   * @since     2.0
   */
  $PMC_Loader_WP_List_Table = new PMC_Loader_WP_List_Table();
